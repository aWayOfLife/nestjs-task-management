import { IsEnum } from 'class-validator';
import { TaskStatus } from '../task.constants';

export class UpdateTaskStatusDto {
  @IsEnum(TaskStatus)
  status: TaskStatus;
}
